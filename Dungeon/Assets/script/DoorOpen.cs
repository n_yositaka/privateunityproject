﻿using UnityEngine;
using System.Collections;

public class DoorOpen : MonoBehaviour {

    private GameObject right_door;
    private GameObject left_door;

    private Light right_light_01;
    private Light right_light_02;
    private Light right_light_03;
    private Light right_light_04;
    private Light right_light_05;

    private Light left_light_01;
    private Light left_light_02;
    private Light left_light_03;
    private Light left_light_04;
    private Light left_light_05;

    private Light light;
    private bool isEnable = false;

    private float startTime = 1.0f;
    private float spanTime = 0.8f;
    private float time = 0.0f;

    public AudioClip audioClip;
    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        right_door = GameObject.Find("stone_door_right");
        left_door  = GameObject.Find("stone_door_left");

        right_light_01 = GameObject.Find("Point light right 01").GetComponent<Light>();
        right_light_02 = GameObject.Find("Point light right 02").GetComponent<Light>();
        right_light_03 = GameObject.Find("Point light right 03").GetComponent<Light>();
        right_light_04 = GameObject.Find("Point light right 04").GetComponent<Light>();
        right_light_05 = GameObject.Find("Point light right 05").GetComponent<Light>();

        left_light_01 = GameObject.Find("Point light left 01").GetComponent<Light>();
        left_light_02 = GameObject.Find("Point light left 02").GetComponent<Light>();
        left_light_03 = GameObject.Find("Point light left 03").GetComponent<Light>();
        left_light_04 = GameObject.Find("Point light left 04").GetComponent<Light>();
        left_light_05 = GameObject.Find("Point light left 05").GetComponent<Light>();

        right_light_01.intensity = 0;
        right_light_02.intensity = 0;
        right_light_03.intensity = 0;
        right_light_04.intensity = 0;
        right_light_05.intensity = 0;
        
        left_light_01.intensity = 0;
        left_light_02.intensity = 0;
        left_light_03.intensity = 0;
        left_light_04.intensity = 0;
        left_light_05.intensity = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if( isEnable ){

            //Debug.Log(Mathf.DeltaAngle(right_door.transform.eulerAngles.y, -90.0f));

            if (Mathf.DeltaAngle(right_door.transform.eulerAngles.y, -90.0f) < -0.1f)
            {
                right_door.transform.Rotate(new Vector3(0f, -2f, 0f));
            }

            if (Mathf.DeltaAngle(left_door.transform.eulerAngles.y, -90.0f) < -0.1f)
            {
                left_door.transform.Rotate(new Vector3(0f, 2f, 0f));
            }

            if (Mathf.DeltaAngle(right_door.transform.eulerAngles.y, -90.0f) > -1.0f)
            {
                time += Time.deltaTime;

                if ((right_light_01.intensity == 0 || left_light_01.intensity == 0) && (time > startTime))
                {
                    right_light_01.intensity = 2;
                    left_light_01.intensity  = 2;
                    startTime = startTime + spanTime;
                }
                else if ((right_light_02.intensity == 0 || left_light_02.intensity == 0) && (time > startTime))
                {
                    right_light_02.intensity = 2;
                    left_light_02.intensity  = 2;
                    startTime = startTime + spanTime;
                }
                else if ((right_light_03.intensity == 0 || left_light_03.intensity == 0) && (time > startTime))
                {
                    right_light_03.intensity = 2;
                    left_light_03.intensity  = 2;
                    startTime = startTime + spanTime;
                }
                else if ((right_light_04.intensity == 0 || left_light_04.intensity == 0) && (time > startTime))
                {
                    right_light_04.intensity = 2;
                    left_light_04.intensity  = 2;
                    startTime = startTime + spanTime;
                }
                else if ((right_light_05.intensity == 0 || left_light_05.intensity == 0) && (time > startTime))
                {
                    right_light_05.intensity = 2;
                    left_light_05.intensity  = 2;
                    startTime = startTime + spanTime;
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if( other.gameObject.tag == "Player" )
        {
            isEnable = true;
            audioSource = gameObject.GetComponent<AudioSource>();
            audioSource.clip = audioClip;
            audioSource.Play();
        }
    }
}
