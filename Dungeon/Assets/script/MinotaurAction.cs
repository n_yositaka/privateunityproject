﻿using UnityEngine;
using System.Collections;

public class MinotaurAction : MonoBehaviour {
    private Animator animation;
    private GameObject target;
    private CharacterController controller;
    private Vector3 moveDirection;
    private Vector3 targetDirection;
    private float speed = 0.05f;
    private float gravity = 20.0f;        
    private bool isEnable =  false;

    private int Idle;
    private int Walk;
    private int Atack;
    private Animator anim;

    private AudioSource[] audioSource;

    void Awake()
    {
        Idle = Animator.StringToHash("Base Layer.IdleWeapon");
        Walk = Animator.StringToHash("Base Layer.Walk");
        Atack = Animator.StringToHash("Base Layer.attack3Weapon");

        anim = GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () {
        animation = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
        audioSource = GetComponents<AudioSource>();
        moveDirection = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
        AnimatorStateInfo anim = animation.GetCurrentAnimatorStateInfo(0);
        //Debug.Log("Flag:" + animation.GetBool("Walk"));

        if( isEnable == true ){
    	    animation.SetBool("Walk", true);
            //audioSource[0].Play();
            //audioSource[1].Play();
            //Debug.Log("walk          :" + Walk);
            //Debug.Log("current anime :" + anim.nameHash);

            if (Walk == anim.nameHash)
            {
                //animation.Play("Walk");
                //attack3Weapon

                // 遠くから近づく
                // 一定距離で攻撃　→　探して向き直り
                // 距離が遠ければ移動 
                Debug.Log("Distance"+Vector3.Distance(transform.position, target.transform.position));
                if (Vector3.Distance(transform.position, target.transform.position) > 4.0)
                {
                    targetDirection = target.transform.position;
                    targetDirection.y = 0;
                    //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirection - transform.position), Time.time * 0.1);

                    moveDirection += transform.forward * 1;
                    moveDirection.y -= gravity * Time.deltaTime;
                    controller.Move(moveDirection * Time.deltaTime * speed);
                }

                if (Vector3.Distance(transform.position, target.transform.position) < 4.5)
                {
                    animation.SetBool("Walk", false);
                    //audioSource[0].Stop();
                    animation.SetBool("Attack", true);
                }
                else
                {
                    animation.SetBool("Attack", false);
                }
            }
        }
        else
        {
            //animation.SetBool("Walk", false);
        }
	}

    void OnTriggerEnter( Collider other ){
        if(other.gameObject.tag == "Player")
        {
             isEnable = true;
             target = other.gameObject;
             //Debug.LogError("audio start");
             //audioSource[1].Play();
        }
    }

    void OnTriggerExit( Collider other ){
        if (other.gameObject.tag == "Player")
        {
            isEnable = false;
        }
    }
}
