﻿using UnityEngine;
using System.Collections;

public class Warp : MonoBehaviour {
    public int warp_x;
    public int warp_z;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter (Collider other) {
        if( other.gameObject.tag == "Player" ){
            //transform.position = new Vector3(0, 0, 0);
            Vector3 pos = other.gameObject.transform.position;

            pos.x = warp_x;
            pos.y = 0;
            pos.z = warp_z;
            other.gameObject.transform.position = pos;
        }
    }
}
