﻿using UnityEngine;
using System.Collections;
using UnityEngine.VR;
using UnityStandardAssets.ImageEffects;

public class PlayerMotor : MonoBehaviour {

    public Vector3 targetDirection;
    private Animator animaotr;
    private Camera camera;
    private CharacterController controller;

    private bool forwardFlg = false;
    private bool backwardFlg = false;
    private bool rightFlg = false;
    private bool leftFlg = false;

	// Use this for initialization
	void Start () {
        //animaotr = GetComponent<Animator>();
        camera = GameObject.Find("Camera").GetComponent<Camera>();
        controller = GetComponent<CharacterController>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey("up") || Input.GetAxis("Vertical") > 0)
        {
            //transform.position += transform.forward * 0.05f;
            //animaotr.SetBool("is_running", true);
            //Debug.Log("up");
            forwardFlg = true;
        }
        else if (Input.GetKey("down") || Input.GetAxis("Vertical") < 0)
        {
            //transform.position += transform.forward * -0.01f;
            //animaotr.SetBool("is_running", true);
            backwardFlg = true;
        }
        else
        {
            //animaotr.SetBool("is_running", false);
        }

        if (Input.GetKey("right") || Input.GetAxis("Horizontal") > 0)
        {
            //transform.Rotate(0, 1, 0);
            rightFlg = true;
        }

        if (Input.GetKey("left") || Input.GetAxis("Horizontal") < 0)
        {
            //transform.Rotate(0, -1, 0);
            leftFlg = true;
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            Vector3 pos = gameObject.transform.position;
            Vector3 rot = gameObject.transform.eulerAngles;
            
            //Debug.Log(pos);
            //Debug.Log(rot);

            pos.x = 1;
            pos.y = 0;
            pos.z = 33;
            gameObject.transform.position = pos;

            rot.x = 0;
            rot.y = 0;
            rot.z = 0;
            gameObject.transform.eulerAngles = rot;

            InputTracking.Recenter();
        }
        /*
        targetDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
         
        //クリックで攻撃
        if( Input.GetButton("Fire1") ){
            GetComponent<Animation>().Play("Attack");
        }//Eでアイテム拾う
        else if( Input.GetKey(KeyCode.E) ){
            GetComponent<Animation>().Play("Pickup");
        }//WASDで移動
        else if( targetDirection.magnitude > 0.1 ){
            GetComponent<Animator>().Play("Run");

            //プレイヤーの向きを変えて
            transform.rotation = Quaternion.LookRotation(targetDirection);

            //CharacterControllerコンポーネントを呼び出し
            CharacterController conroller = GetComponent<CharacterController>();

            //移動
            conroller.Move(transform.forward * Time.deltaTime * 3f);
        }
        else {
            //何も押されてない場合はIdle
            GetComponent<Animation>().Play("Idle");
        }
        */
    }

    void FixedUpdate () {
        Transform t = camera.transform;
        Vector3 work;

        //Debug.Log("player : " + transform.rotation.y + " / camera : " + t.rotation.y + " / depth : " + (transform.rotation.y - t.rotation.y));
        /*
        if( (transform.rotation.y - t.rotation.y) > 0.2 )
        {
            //Debug.Log("turn right.");
            transform.Rotate(new Vector3(0f, 0.2f, 0f));
        }

        if( (transform.rotation.y - t.rotation.y) < -0.1)
        {
            //Debug.Log("turn left.");
            transform.Rotate(new Vector3(0f, -0.2f, 0f));
        }
        */

        if( forwardFlg )
        {
            work = t.forward * 0.04f;
            //transform.position += new Vector3(work.x, 0, work.z);
            controller.Move(new Vector3(work.x, 0, work.z));
            //animaotr.SetBool("is_running", true);
            forwardFlg = false;
        }

        if( backwardFlg )
        {
            work = t.forward * -0.01f;
            //transform.position += new Vector3(work.x, 0, work.z);
            controller.Move(new Vector3(work.x, 0, work.z));
            //animaotr.SetBool("is_running", true);
            backwardFlg = false;
        }

        if( rightFlg )
        {
            transform.Rotate(new Vector3(0f, 0.2f, 0f));
            rightFlg = false;
        }

        if( leftFlg )
        {
            transform.Rotate(new Vector3(0f, -0.2f, 0f));
            leftFlg = false;
        }
    }
}
