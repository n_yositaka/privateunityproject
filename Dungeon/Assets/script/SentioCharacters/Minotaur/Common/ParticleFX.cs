﻿using UnityEngine;
using System.Collections.Generic;

namespace Sentio.Minotaur
{
    public class ParticleFX : MonoBehaviour
    {
        public string effectName;
        public Animator animator = null;
        public string parameterName = null;
        public float parameterThreshold = 0f;

        public GameObject effectObject;
        public bool spawnInstance;
        public float destroyTime = 1f;
        public float minSpawnDelay = 0.5f;
        public Vector3 positionOffset = Vector3.zero;
        public Vector3 rotationOffset = Vector3.zero;

        private float tLastSpawn = 0f;

        public void EffectEvent(string aEffect)
        {
            if (aEffect == effectName)
            {
                try
                {
                    if ((animator == null) || string.IsNullOrEmpty(parameterName) || animator.GetFloat(parameterName) >= parameterThreshold)
                    {
                        if (spawnInstance)
                        {
                            if ((Time.realtimeSinceStartup - tLastSpawn) > minSpawnDelay)
                            {
                                GameObject tObject = GameObject.Instantiate(effectObject, transform.position + positionOffset, transform.rotation * Quaternion.Euler(rotationOffset)) as GameObject;
                                tObject.SetActive(true);
                                if (!tObject.GetComponent<ParticleSystem>().isPlaying)
                                    tObject.GetComponent<ParticleSystem>().Play();
                                Destroy(tObject.gameObject, destroyTime);

                                tLastSpawn = Time.realtimeSinceStartup;
                            }
                        }
                        else
                            effectObject.GetComponent<ParticleSystem>().Play();
                    }
                }
                catch { }
            }
        }
    }
}