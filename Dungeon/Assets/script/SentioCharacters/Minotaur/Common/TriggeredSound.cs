﻿using UnityEngine;
using System.Collections;

namespace Sentio.Minotaur
{
    public class TriggeredSound : MonoBehaviour
    {
        public void TriggerSound(string aSound)
        {
            BroadcastMessage("SoundEvent", aSound, SendMessageOptions.DontRequireReceiver);
        }
    }
}