using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Sentio.Minotaur
{
    public class AreaDamageEffect : MonoBehaviour
    {
        public float damage;
        public float effectTime;
        public float maxRadius;
        public SphereCollider areaDamageCollider;
        public DamageOnCollision damageOnCollision;

        private float _minEffectTime = 0.1f;
        private float _minRadius = 0.001f;
        private float _timer = 0f;
        private bool _succeedNextFrame = false;

        public void Start()
        {
            _timer = 0f;
            areaDamageCollider.radius = _minRadius;
            damageOnCollision.canDamageSelf = false;
            damageOnCollision.canSendDamage = true;
            damageOnCollision.BaseDamageAmount = damage;
            _succeedNextFrame = false;
        }

        public void Update()
        {
            //We delay one frame after reaching max radius to allow physics a chance to process
            if (_succeedNextFrame)
            {
                damageOnCollision.canSendDamage = false;
                return;
            }

            areaDamageCollider.transform.position += Vector3.zero; //hack to force onTriggerEnter events for a non-moving object
            effectTime = Mathf.Max(_minEffectTime, effectTime);
            areaDamageCollider.radius = Mathf.Max(_minRadius, Mathf.Clamp01(_timer / effectTime) * maxRadius);

            _timer += Time.deltaTime;
            if (_timer >= effectTime)
                _succeedNextFrame = true;
        }
    }
}