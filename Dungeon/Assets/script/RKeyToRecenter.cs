﻿using UnityEngine;
using System.Collections;
using UnityEngine.VR;

public class RKeyToRecenter : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //VRSettings.renderScale = 0.5f;
        InputTracking.Recenter();
	}
	
	// Update is called once per frame
	void Update () {
        // Sキーでシーンををリセットする
        if (Input.GetKeyDown(KeyCode.S))
        {
            Application.LoadLevel("copy_main");
        }

        // Rキーで位置トラッキングをリセットする
        if (Input.GetKeyDown(KeyCode.R))
        {
            InputTracking.Recenter();
        }

        // Escキーでアプリケーションを終了する
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
	}
}
