﻿using UnityEngine;
using System.Collections;

public class MapGenerator : MonoBehaviour {

	public GameObject wallPrefab;
	public GameObject defaultWallPrefab;
    public GameObject skeletonPrefab;
	private int default_x_max = 19;
	private int default_z_max = 19;
		
	// Use this for initialization
	void Start () {
		//これがマップの元になるデータ
        /*
        string map_matrix  = "022220000000000000:";
               map_matrix += "022220000000000000:";
               map_matrix += "022220000000000000:";
               map_matrix += "022221111111111110:";
               map_matrix += "022220000000000010:";
               map_matrix += "000100000000000010:";
               map_matrix += "000110000000000010:";
               map_matrix += "000010022200000010:";
               map_matrix += "000010035200000010:";
               map_matrix += "000010022200000010:";
               map_matrix += "000011122200000010:";
               map_matrix += "000000025200000010:";
               map_matrix += "000000000000000010:";
               map_matrix += "000000000000002222:";
               map_matrix += "000000000000115222:";
               map_matrix += "000000000000002222:";
               map_matrix += "000000000000000000:";
               map_matrix += "000000000000000000";
         */
        string map_matrix  = "111111000000000000:";
               map_matrix += "100001000000000000:";
               map_matrix += "100001111111111111:";
               map_matrix += "100000000000000001:";
               map_matrix += "100001111111111101:";
               map_matrix += "111011000000000101:";
               map_matrix += "001001111110000101:";
               map_matrix += "001101100010000101:";
               map_matrix += "000101105010000101:";
               map_matrix += "000101100010000101:";
               map_matrix += "000100000010000101:";
               map_matrix += "000111105010000101:";
               map_matrix += "000000111110111101:";
               map_matrix += "000000000011100001:";
               map_matrix += "000000000010050001:";
               map_matrix += "000000000011100001:";
               map_matrix += "000000000000111111:";
               map_matrix += "000000000000000000";

		// 枠を作るメソッド呼び出し
		//CreateMapWaku();

		// 引数にこれを入れてマップ生成する
		CreateMap(map_matrix);
	}
		
	// 枠を作るメソッド
	void CreateMapWaku() {
        // ループしながらz軸の上と下２列に枠を作ります
		for( int dx = 0; dx <= default_x_max; dx++ ){
			Instantiate(defaultWallPrefab, new Vector3(dx, 0, 0), Quaternion.identity);
            Instantiate(defaultWallPrefab, new Vector3(dx, 1, 0), Quaternion.identity);
			Instantiate(defaultWallPrefab, new Vector3(dx, 0, default_z_max), Quaternion.identity);
            Instantiate(defaultWallPrefab, new Vector3(dx, 1, default_z_max), Quaternion.identity);
		}

		// 同じくx軸に右と左に枠を作ります
		for( int dz = 0; dz <= default_z_max; dz++ ){
			Instantiate(defaultWallPrefab, new Vector3(0, 0, dz), Quaternion.identity);
            Instantiate(defaultWallPrefab, new Vector3(0, 1, dz), Quaternion.identity);
			Instantiate(defaultWallPrefab, new Vector3(default_x_max, 0, dz), Quaternion.identity);
            Instantiate(defaultWallPrefab, new Vector3(default_x_max, 1, dz), Quaternion.identity);
		}
	}

	// マップを作るメソッド
	void CreateMap(string map_matrix) {
		//「:」をデリミタとして、mapp_matrix_arrに配列として分割していれます
		string[] map_matrix_arr = map_matrix.Split(':');
		
		// map_matrix_arrの配列の数を最大値としてループ
		for( int x = 0; x < map_matrix_arr.Length; x++ ){
            // xを元に配列の要素を取り出す
			string x_map = map_matrix_arr[x];

            // 配列に格納されている文字の数でx軸をループ
			for( int z = 0; z < x_map.Length; z++ ){
				// 配列から取り出した1要素を1文字づつ取り出す
				int obj = int.Parse(x_map.Substring(z, 1));
				
				// もしも1だったら壁ということで壁のプレハブをインスタンス化してループして出したx座標z座標を指定して設置
				if( obj == 1 ){
				    GameObject wall = Instantiate(wallPrefab, new Vector3(x, 1, z), Quaternion.identity) as GameObject;
                    wall.transform.localScale = new Vector3(1, 3, 1);
                    wall.GetComponent<Renderer>().material.mainTextureScale = new Vector2(1, 3);
				}

                if (obj == 5) {
                    Instantiate(skeletonPrefab, new Vector3(x, 0, z), Quaternion.identity);
                }

                //Instantiate(wallPrefab, new Vector3(x + 1, 2, z + 1), Quaternion.identity);
			}
		}
	}

	// Update is called once per frame
	void Update () {
	}
}
