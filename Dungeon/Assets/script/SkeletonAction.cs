﻿using UnityEngine;
using System.Collections;

public class SkeletonAction : MonoBehaviour {
    private Animation animation;
    private GameObject target;
    private float defaultSpeed;
    private Vector3 startPosition;
    private float positionDistance;
    private bool ResurrectionFlg = false;
    private bool isEnable = false;
    
	// Use this for initialization
	void Start () {
        animation = GetComponent<Animation>();
        defaultSpeed = animation["Resurrection"].speed;
        startPosition = animation.transform.position;
        animation.transform.Translate(new Vector3(0, -0.5f, 0));
        positionDistance = Vector3.Distance(animation.transform.position, startPosition);
        animation.Play("Resurrection");
        animation["Resurrection"].speed = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (!ResurrectionFlg)
        {
            if (isEnable)
            {
                animation["Resurrection"].wrapMode = WrapMode.ClampForever;
                ResurrectionFlg = true;
                animation["Resurrection"].speed = defaultSpeed;
                animation.CrossFade("Resurrection");
            }
        }
        else {
            if (Vector3.Distance(animation.transform.position, startPosition) <= positionDistance)
            {
                positionDistance = Vector3.Distance(animation.transform.position, startPosition);
                animation.transform.Translate(Vector3.up * 0.1f * Time.deltaTime);
            }

            if (animation["Resurrection"].length + 0.2f <= animation["Resurrection"].time)
            { 
                animation["Idle2"].wrapMode = WrapMode.Loop;
                animation.CrossFade("Idle2");
            }
        }
	}

    void OnTriggerEnter(Collider colobj){
        if( colobj.tag == "Player"){
            isEnable = true;
            target = colobj.gameObject;
        }
    }
}
