﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour {

    private GameObject _child;

	// Use this for initialization
	void Start () {
        _child = transform.FindChild("Canvas").gameObject;
        _child.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            _child.SetActive(true);
        }
    }
}
