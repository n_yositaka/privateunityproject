**********************************************************************************************
==============  Thank you for purchasing Modular Cave Assets - Package 1!  ===================

**********************************************************************************************

------------------------------------------------------
*Contents of Package, Information and Version Updates*
------------------------------------------------------




|========|
|Contents|
|========|


FBX  - A folder containing FBX files

Materials - Materials for each asset are stored here

Prefabs - These are the game ready prefabs for use within your scene

PSDs - Each diffuse texture is found here in PSD format for manual editing

Textures - Optimised TGA files for each asset.  



*The materials are linked to their relevant meshes, as some meshes share identical UV co-ordinates*


.PSD files are also included within the package if you wish to alter the textures manually 


---------------------------------------------------------------------------------------------



|===========|
|Information|
|===========|


All assets have been pre-mapped and light-mapped, so no alteration is necessary for 
in-game placement and use.


However, if you wish to make your own instances of the rock walls etc, importing the .FBX to a 3D package will allow you to make changes to the meshes.*


Textures have been optimised for in engine use, but if you wish to save at different sizes
and/or in different formats, please find the .PSD files included within this package.



This package has been designed so that you are able to create your own unique prefabs as well
as utilise the pre-made examples contained within the package and scene.


Each of the pre-made bridges, rope ladders and ledges come with collider meshes, which have been turned off as renderable. You can keep these collider meshes if you wish, or delete them from the mesh as appropriate.


*= Please do not alter these meshes and re-release to the asset store.

---------------------------------------------------------------------------------------------



|===============|
|Version Updates|
|===============|

V1.0.0 


*This is the initial Release of this package, notes on any future updates can be found
 here*





**********************************************************************************************

==============================================================================================

**********************************************************************************************





