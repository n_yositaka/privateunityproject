﻿#pragma strict
var flg:int = 1;

function Update(){
    var thespikes = gameObject.FindWithTag("spikes");
    var anim:Animation = thespikes.GetComponent.<Animation>();

    for( var state : AnimationState in anim ){
        if( state.time >= 0.8 ){
            anim.Stop("spikes");
        }
    }
}


function OnTriggerEnter (obj : Collider) {
    var thespikes = gameObject.FindWithTag("spikes");
    var anim:Animation = thespikes.GetComponent.<Animation>();

    if( flg ){
        for( var state : AnimationState in anim ){
            state.speed = 0.3;
        }

        anim.Play("spikes");
        flg = 0;
    }
}