﻿using UnityEngine;
using System.Collections;
using UnityEngine.VR;

public class Player : MonoBehaviour {
    NavMeshAgent agent;

    private Camera camera;
    private CharacterController controller;

    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        camera = GameObject.Find("CenterEyeAnchor").GetComponent<Camera>();
        controller = GetComponent<CharacterController>();

    }
	
	// Update is called once per frame
	void Update () {
        Vector3 t = camera.transform.localPosition;
        float forward = Mathf.Round(t.z * 100.0f) / 100.0f;
        float horizontal = Mathf.Round(t.x * 100.0f) / 100.0f;

        //Debug.Log("debug:" + forward + "/" + horizontal);

        float new_z = 0;
        if ( 0.02 < forward)
        {
            new_z = forward + 0.02f;
        }

        float new_x = 0;
        if (0.02 < horizontal || -0.02 > horizontal )
        {
            new_x = horizontal / 2;
        }

        Vector3 f = camera.transform.TransformDirection(Vector3.forward);
        Vector3 r = camera.transform.TransformDirection(Vector3.right);
        //Vector3 move = new Vector3(new_x, 0, new_z);
        Vector3 move = new_x * r + new_z * f;
        //agent.Move(move * Time.deltaTime * 60);
        controller.Move(move * Time.deltaTime * 60);
    }
}
