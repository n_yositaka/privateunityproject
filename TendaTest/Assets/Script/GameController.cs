﻿using UnityEngine;
using System.Collections;
using UnityEngine.VR;

public class GameController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        VRSettings.renderScale = 1.0f;
        InputTracking.Recenter();
    }
	
	// Update is called once per frame
	void Update () {
        // Sキーでシーンををリセットする
        if (Input.GetKeyDown(KeyCode.S))
        {
            Application.LoadLevel("Main");
        }

        // Rキーで位置トラッキングをリセットする
        if (Input.GetKeyDown(KeyCode.R))
        {
            InputTracking.Recenter();
        }

        // Escキーでアプリケーションを終了する
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
