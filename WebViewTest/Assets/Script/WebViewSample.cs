﻿using UnityEngine;
//using System.Collections;

public class WebViewSample : MonoBehaviour {
    private string url = "http://www.tenda.co.jp/";

    // Use this for initialization
    void Start () {
        WebViewObject webViewObject = (new GameObject("WebViewObject")).AddComponent<WebViewObject>();
        webViewObject.Init((msg) => {
            Debug.Log(msg);
        });

        webViewObject.LoadURL(url);
        webViewObject.SetMargins(20, 20, 20, 100);
        webViewObject.SetVisibility(true);
    }

    void Update () {

    }
}
